var app = app || {};

var spBreak = 767;

app.init = function () {

  app.slick();
  app.lightbox();
  app.odometer();
  app.navigation();
  app.registerBtn();
  app.smooth();
  app.formRegistration();

};

app.isMobile = function () {
  return window.matchMedia('(max-width: ' + spBreak + 'px)').matches;
};

app.slick = function () {

  if($('.js-slider').length) {
    $('.js-slider').each(function(){
      var slickDots = $('#js-dots');
      $('.js-slider').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        dots: true,
        speed: 800,
        arrows: true,
        fade: true,
        appendDots: slickDots,
        appendArrows: slickDots,
      });
    });
   }

};

app.navigation = function() {

  var btnMenu = $('#btn-menu');
  var spNav = $('#navigation');
  var offsetY = window.pageYOffset;

  btnMenu.click(function () {
    btnMenu.toggleClass('is-active');
    if (btnMenu.hasClass('is-active')) {
      spNav.slideDown();
      $('#overlay-menu').show();
      offsetY = window.pageYOffset;
      $('body').css({
        position: 'fixed',
        'top': -offsetY + 'px',
        width: '100%'
      });
    } else {
      spNav.slideUp();
      $('#overlay-menu').hide();
      $('body').css({
        'position': 'static',
        'top': 'auto',
        'width': 'auto'
      });
      $(window).scrollTop(offsetY);
    }
    return false;
  });

  $('#overlay-menu').click(function () {
    $(this).hide();
    spNav.slideUp();
    btnMenu.removeClass('is-active');
    $('body').css({
      'position': 'static',
      'top': 'auto',
      'width': 'auto'
    });
    $(window).scrollTop(offsetY);
  });

  $('#navigation a.js-scroll').click(function () {
    $('#overlay-menu').hide();
    spNav.slideUp();
    btnMenu.removeClass('is-active');
    $('body').css({
      'position': 'static',
      'top': 'auto',
      'width': 'auto'
    });
    $(window).scrollTop(offsetY);
  });

};

app.lightbox = function () {

  var module = this;
    module.$lightbox = $(".lightbox");
    module.$btn = $(".open-lightbox");

    module.init = function() {

        module.$btn.click(function(){
            var lightboxID = $(this).attr("data-lightbox");
            module.open(lightboxID)
        });

        module.$lightbox.find(".close").click(function() {
            var $lightbox = $(this).parents(".lightbox");
            module.close($lightbox);
        });

    }

     module.open = function( lightboxID ) {

         $(".lightbox[data-lightbox='"+lightboxID+"']").fadeIn("fast", function() {
             $("body, .container").addClass("no-scroll");
             $(document).on("touchmove", function(e) {
                 if( $(e.target).parents(".window").length==0 ) return false;
             });
         });

     }

     module.close = function( $lightbox ) {

         $("body, .container").removeClass("no-scroll");
         $lightbox.fadeOut('fast');
         $(document).off("touchmove");

     }

     if( module.$lightbox.length > 0 ) module.init();

};

app.registerBtn = function () {

  var btnRegister = $('#register-btn');

  btnRegisterFade();

  $(window).on('load scroll resize', function () {
    btnRegisterFade();
  });

  function btnRegisterFade () {
    if ($(window).scrollTop() > $(window).height() * 0.2) {
      if (!btnRegister.is(':visible')) {
        btnRegister.css('opacity', 0).show();
        btnRegister.animate({
          opacity: 1
        }, 400);
      }
    } else {
      if (btnRegister.is(':visible') && !btnRegister.is(':animated')) {
        btnRegister.animate({
          opacity: 0
        }, 400, function () {
          btnRegister.css('opacity', 1).hide();
        });
      }
    }
  }

};
app.smooth = function () {
  var anchorElm = $('.js-scroll');
  anchorElm.click(function() {
    var href = $(this).attr('href').split('#')[1],
      target = $(href == "" ? 'html' : '#' + href);
    if ($('#' + href).length) {
      var position = target.offset().top;
      $('body,html').animate({
        scrollTop: position
      }, 500, 'swing');
      return false;
    }
  });

};

app.formRegistration = function () {
  $('.form-registration').submit(function() {
    $(".lightbox[data-lightbox='lightbox']").fadeIn("fast", function() {
      $("body, .container").addClass("no-scroll");
    });
    return false;
  });
};

app.odometer = function() {

  var value = Array(500000000, 300000000, 100000000, 30000000);
  setInterval(function () {
    var randomValue = value[Math.floor(Math.random() * value.length)];
    $('.odometer').html(Math.floor(Math.random() * 1000000000));
    $('.odometer').html(randomValue);
  }, 6000);

};

app.updateKv = function() {

  if (!app.isMobile()) {
    var i = 1;
    setInterval(function () {
      $('.section-keyvisual img.md').attr('src', 'img/top/keyvisual_img_0' + i + '.jpg');
      if (i < 3) {
        i++;
      } else {
        i = 1
      }
    }, 200);
  } else {
    var i = 1;
    setInterval(function () {
      $('.section-keyvisual img.sm').attr('src', 'img/top/sp/keyvisual_img_0' + i + '.jpg');
      if (i < 3) {
        i++;
      } else {
        i = 1
      }
    }, 200);
  }

};


$(function () {

  app.init();

});

$(window).on('load', function () {
  app.updateKv();
})
